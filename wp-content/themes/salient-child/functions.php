<?php 


	function yoga_register() {  
	    	 
		 $yoga_labels = array(
		 	'name' => __( 'Yoga', 'taxonomy general name', NECTAR_THEME_NAME),
			'singular_name' => __( 'Yoga Item', NECTAR_THEME_NAME),
		 );
		 
		 global $options;
	     $custom_slug = null;		
		 
		 if(!empty($options['yoga_rewrite_slug'])) $custom_slug = $options['yoga_rewrite_slug'];
		
		 $portolfio_menu_icon = (floatval(get_bloginfo('version')) >= "3.8") ? 'dashicons-universal-access-alt' : NECTAR_FRAMEWORK_DIRECTORY . 'assets/img/icons/portfolio.png';
		
		 $args = array(
				'labels' => $yoga_labels,
				'rewrite' => array('slug' => $custom_slug,'with_front' => false),
				'singular_label' => __('Project', NECTAR_THEME_NAME),
				'public' => true,
				'publicly_queryable' => true,
				'show_ui' => true,
				'hierarchical' => false,
				'menu_position' => 9,
				'menu_icon' => $portolfio_menu_icon,
				'supports' => array('title', 'editor', 'thumbnail', 'revisions'),  
				'taxonomies'  => array( 'category' ),

	       );  
	  
	    register_post_type( 'yoga' , $args );  
	}  
	add_action('init', 'yoga_register', 0);


	function writings_register() {  
	    	 
		 $writings_labels = array(
		 	'name' => __( 'Writings', 'taxonomy general name', NECTAR_THEME_NAME),
			'singular_name' => __( 'writings Item', NECTAR_THEME_NAME),
		 );
		 
		 global $options;
	     $custom_slug = null;		
		 
		 if(!empty($options['writings_rewrite_slug'])) $custom_slug = $options['writings_rewrite_slug'];
		
		 $portolfio_menu_icon = (floatval(get_bloginfo('version')) >= "3.8") ? 'dashicons-format-aside' : NECTAR_FRAMEWORK_DIRECTORY . 'assets/img/icons/portfolio.png';
		
		 $args = array(
				'labels' => $writings_labels,
				'rewrite' => array('slug' => $custom_slug,'with_front' => false),
				'singular_label' => __('Project', NECTAR_THEME_NAME),
				'public' => true,
				'publicly_queryable' => true,
				'show_ui' => true,
				'hierarchical' => false,
				'menu_position' => 9,
				'menu_icon' => $portolfio_menu_icon,
				'supports' => array('title', 'editor', 'thumbnail', 'revisions')  

	       );  
	  
	    register_post_type( 'writings' , $args );  
	}  
	add_action('init', 'writings_register', 0);


	function quotes_register() {  
	    	 
		 $quotes_labels = array(
		 	'name' => __( 'Quotes', 'taxonomy general name', NECTAR_THEME_NAME),
			'singular_name' => __( 'Quotes Item', NECTAR_THEME_NAME),
		 );
		 
		 global $options;
	     $custom_slug = null;		
		 
		 if(!empty($options['quotes_rewrite_slug'])) $custom_slug = $options['quotes_rewrite_slug'];
		
		 $portolfio_menu_icon = (floatval(get_bloginfo('version')) >= "3.8") ? 'dashicons-format-quote' : NECTAR_FRAMEWORK_DIRECTORY . 'assets/img/icons/portfolio.png';
		
		 $args = array(
				'labels' => $quotes_labels,
				'rewrite' => array('slug' => $custom_slug,'with_front' => false),
				'singular_label' => __('Project', NECTAR_THEME_NAME),
				'public' => true,
				'publicly_queryable' => true,
				'show_ui' => true,
				'hierarchical' => false,
				'menu_position' => 9,
				'menu_icon' => $portolfio_menu_icon,
				'supports' => array('title', 'editor', 'thumbnail', 'revisions')  
	       );  
	  
	    register_post_type( 'quotes' , $args );  
	}  
	add_action('init', 'quotes_register', 0);



add_action( 'wp_enqueue_scripts', 'salient_child_enqueue_styles');
function salient_child_enqueue_styles() {
	
    wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css', array('font-awesome'));

    if ( is_rtl() ) 
   		wp_enqueue_style(  'salient-rtl',  get_template_directory_uri(). '/rtl.css', array(), '1', 'screen' );
}

?>